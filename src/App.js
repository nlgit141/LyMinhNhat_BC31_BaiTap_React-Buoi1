// import logo from './logo.svg';
import './App.css';
import Header from './BaitapThucHanhLayout/Header/Header';
import Body from './BaitapThucHanhLayout/Body/Body';
import Footer from './BaitapThucHanhLayout/Footer/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}
export default App;
